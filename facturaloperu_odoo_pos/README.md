FacturaloPeru | Odoo | Módulo de POS
====================================

Módulo desarrollado para Odoo 11.0 con conexion a la API del Facturador PRO


Instalación
-----------

Se instala de manera automatica. Puede observar el proceso realizado a través del repositorio: https://gitlab.com/rash07/facturaloperu_odoo_base

Configuración
-------------

Puede visitar el siguiente enlace para ver el manual de instalación y configuración: https://docs.google.com/document/d/188SdLQVzgUz0ybVFWHJje_BPfmsfepJOMYZpWEgg3tY/edit?usp=sharing

Funcionalidad
-------------

Puede visitar el siguiente enlace para ver la guía de usuario: https://docs.google.com/document/d/18BzE_g-Gc2f-boGweDl4Qpm-Q0UTd1LGNonXxQHKPfk/edit?usp=sharing

Autor
-----

**FacturaloPeru** http://facturaloperu.com
odoo.define('ncf_pos.chrome', function (require) {
    "use strict";
    var chrome = require("point_of_sale.chrome");

    chrome.SynchNotificationWidget = chrome.SynchNotificationWidget.include({
        start: function(){
            var self = this;
            this.pos.bind('change:synch', function(pos,synch){
                self.set_status(synch.state, synch.pending);
            });
            this.$el.click(function(){
                self.pos.push_and_invoice_order(null,{'show_error':true});
            });
        },
    });
});

odoo.define('facturaloperu_odoo_pos.pos_screens', function(require) {
  "use strict";

  var screens = require('point_of_sale.screens');
  var models = require('point_of_sale.models');
  var PaymentScreenWidget = screens.PaymentScreenWidget;

  var _super_order = models.Order.prototype;

  models.PosModel.prototype.models.some(function (model) {
    if (model.model !== 'res.company') {
      return false;
    }
    ['street','city', 'state_id', 'province_id', 'district_id', 'ubigeo'].forEach(function (field) {
      if (model.fields.indexOf(field) == -1) {
        model.fields.push(field);
      }
    });
    return true;
  });

  models.PosModel.prototype.models.push({
    model: 'einvoice.catalog.06',
    fields: [
      'name',
      'code'
    ],
    domain:  function(self){
      return [];
    },
    context: function(self){
      return {};
    },
    loaded: function(self, doc_types){
      self.doc_types = doc_types;
    },
  });

  models.PosModel.prototype.models.push({
    model: 'pos.order.document.type',
    fields: [
      'name',
      'number',
    ],
    domain:  function(self){
      return [];
    },
    context: function(self){
      return {};
    },
    loaded: function(self, pos_doc_types){
      self.pos_doc_types = pos_doc_types;
    },
  });

  models.PosModel.prototype.models.push({
    model: 'account.journal',
    fields: [
      'name',
      'sequence_number_next',
      'code',
      'company_id',
    ],
    domain:  function(self){
      return [];
    },
    context: function(self){
      return {};
    },
    loaded: function(self, pos_account_journal){
      self.pos_account_journal = pos_account_journal;
    },
  });

  models.PosModel.prototype.models.push({
    model: 'pos.order.serial.number',
    fields: [
    'name',
    'document_type_id',
    'document_type_name',
    'document_type_number',
  ],
    domain:  function(self){
      return [];
    },
    context: function(self){
      return {};
    },
    loaded: function(self, serial_numbers){
      self.serial_numbers = serial_numbers;
    },
  });

  models.load_fields('res.partner', ['doc_number', 'catalog_06_id', 'state', 'ubigeo'])
  models.load_fields('pos.order', ['api_external_id', 'api_number_feapi', 'api_link_cdr', 'api_link_pdf', 'api_link_xml','iface_invoicing_second','invoicing_second','el_numero'])

  models.Order = models.Order.extend({
    initialize: function() {
      _super_order.initialize.apply(this, arguments);
      var order = this.pos.get_order();
      this.api_number_feapi = this.api_number_feapi || '';
      this.api_external_id = this.api_external_id || '';
      this.api_link_cdr = this.api_link_cdr || '';
      this.api_link_xml = this.api_link_xml || '';
      this.api_link_pdf = this.api_link_pdf || '';
      this.api_code_json = this.api_code_json || '';
      this.api_qr = this.api_qr || '';
      this.serial_number = this.serial_number || '';
      this.document_type_number = this.document_type_number || '';
      this.iface_invoicing_second = this.iface_invoicing_second || '';
      this.invoicing_second = this.invoicing_second || '';
      this.save_to_db();
      return this
    },
    export_as_JSON: function() {
      var json = _super_order.export_as_JSON.apply(this,arguments);
      json.api_external_id = this.api_external_id;
      json.api_number_feapi = this.api_number_feapi;
      json.api_link_cdr = this.api_link_cdr;
      json.api_link_xml = this.api_link_xml;
      json.api_link_pdf = this.api_link_pdf;
      json.api_code_json = this.api_code_json;
      json.api_qr = this.api_qr;
      json.iface_invoicing_second = this.iface_invoicing_second;
      json.invoicing_second = this.invoicing_second;
      return json;
    },
  });

  screens.ClientListScreenWidget.include({
    save_client_details: function(partner) {
      var self = this;
      var fields = {};
      this.$('.client-details-contents .detail').each(function(idx,el){
        fields[el.name] = el.value || false;
      });

      if (fields.vat) {
        fields.doc_number = fields.vat;
      }

      if (!fields.doc_number || !fields.catalog_06_id || !fields.vat) {
        this.gui.show_popup(
          'error',
          'Debe llenar los campos de Tipo de Documento y Número de Documento en el formulario del cliente.'
        );
        return;
      }
      this._super(partner);
    },
  });

  PaymentScreenWidget.include({
    get_document_type_number: function(serial_number_selected) {
      var document_type_number = ''
      for (var i = 0; i < this.pos.serial_numbers.length; i++) {
        var serial_number = this.pos.serial_numbers[i];
        if (serial_number.name == serial_number_selected) {
          document_type_number = serial_number.document_type_number;
          break;
        }
      }
      return document_type_number;
    },
    click_invoice_second: function(){
      var self = this;
      var order = this.pos.get_order();

      // numero documento to invoicing_second

      var journals = self.pos.pos_account_journal;
      var journal_id = '';

      var reg_boleta = journals.find(function(element) {
        return element.code == self.pos.config.iface_boleta_serial_number && element.company_id[0] == self.pos.config.company_id[0];
      });

      journal_id = reg_boleta.id;

      setTimeout(function() {
        var search = [['id', '=', journal_id]];
        self._rpc({
          model: 'account.journal',
          method: 'search_read',
          domain: search,
          fields: ['name','code','sequence_number_next'],
        }).then(function(res) {
          console.log(res[0].sequence_number_next)
          order.invoicing_second = res[0].sequence_number_next;
        }).fail(function(err) {
          console.log('no se encontro el diario');
        })
      }, 4000);

      var domain = [['id', '=', this.pos.config.id]];
      setTimeout(function() {
        // busca la configuracion
        self._rpc({
          model: 'pos.config',
          method: 'search_read',
          domain: domain,
          fields: ['id','invoice_second_journal_id','invoice_journal_id'],
        }).done(function(o) {
          // si no hay respuesta
          if (o.length == 0) {
            console.log('error al consultar config.pos');
          } else {
            console.log(o);
            // asigno valores a las variables
            var first_id = o[0].invoice_journal_id[0];
            var second_id = o[0].invoice_second_journal_id[0];
            var f = o[0].invoice_journal_id[1].search("Boleta");

            // si el name de first_id no contiene boleta
            if (f == -1) {
              // preparo el intercambio
              var data_json = {
                invoice_journal_id: second_id,
                invoice_second_journal_id: first_id
              }
              // guardo los cambios
              self._rpc({
                model: 'pos.config',
                method: 'write',
                args: [[o[0].id], data_json],
              }).done(function() {
                new Noty({
                  theme: 'bootstrap-v4',
                  type: 'success',
                  timeout: 5000,
                  layout: 'topRight',
                  text: '<h3>Se ha preparado la boleta</h3>'
                }).show();
              }).fail(function(err) {
                new Noty({
                  theme: 'bootstrap-v4',
                  type: 'warning',
                  timeout: 5000,
                  layout: 'topRight',
                  text: '<h3>No se ha podido configurar para boleta</h3>'
                }).show();
              })
            } else {
              new Noty({
                theme: 'bootstrap-v4',
                type: 'success',
                timeout: 5000,
                layout: 'topRight',
                text: '<h3>Se ha preparado la boleta</h3>'
              }).show();
            }
          }

        }).fail(function(err) {
          new Noty({
            theme: 'bootstrap-v4',
            type: 'warning',
            timeout: 5000,
            layout: 'topRight',
            text: '<h3>Ha caducado el tiempo de configuración</h3>'
          }).show();
          def.reject()
        });

      }, 3000);

      if ($(".js_invoice").hasClass("highlight")) {
        order.set_to_invoice(!order.is_to_invoice());
        $(".js_invoice").removeClass("highlight");
      }

      order.set_to_invoice(!order.is_to_invoice());
      if (order.is_to_invoice()) {
        this.$('.js_invoice_second').addClass('highlight');
      } else {
        this.$('.js_invoice_second').removeClass('highlight');
      }

      function wait(ms)
      {
        var d = new Date();
        var d2 = null;
        do {
          d2 = new Date();
        }
        while(d2-d < ms);
      }
      wait(4000);
    },
    click_invoice: function(){
      var self = this;
      var order = this.pos.get_order();

      // numero documento to invoicing_second

      var journals = self.pos.pos_account_journal;
      var journal_id = '';

      var reg_factura = journals.find(function(element) {
        return element.code == self.pos.config.iface_factura_serial_number && element.company_id[0] == self.pos.config.company_id[0];;
      });

      journal_id = reg_factura.id;

      setTimeout(function() {
        var search = [['id', '=', journal_id]];
        self._rpc({
          model: 'account.journal',
          method: 'search_read',
          domain: search,
          fields: ['name','code','sequence_number_next'],
        }).then(function(res) {
          console.log(res[0].sequence_number_next)
          order.invoicing_second = res[0].sequence_number_next;
        }).fail(function(err) {
          console.log('no se encontro el diario');
        })

      }, 4000);

      var domain = [['id', '=', this.pos.config.id]];
      setTimeout(function() {
        // busca la configuracion
        self._rpc({
          model: 'pos.config',
          method: 'search_read',
          domain: domain,
          fields: ['id','invoice_second_journal_id','invoice_journal_id','name'],
        }).done(function(o) {
          // si no hay respuesta
          if (o.length == 0) {
            console.log('error al consultar config.pos');
          } else {
            console.log(o);
            // asigno valores a las variables
            var first_id = o[0].invoice_journal_id[0];
            var second_id = o[0].invoice_second_journal_id[0];

            var f = o[0].invoice_journal_id[1].search("Factura");

            if (f == -1) {
              // preparo el intercambio
              var data_json = {
                invoice_journal_id: second_id,
                invoice_second_journal_id: first_id
              }
              // guardo los cambios
              self._rpc({
                model: 'pos.config',
                method: 'write',
                args: [[o[0].id], data_json],
              }).done(function() {
                new Noty({
                  theme: 'bootstrap-v4',
                  type: 'success',
                  timeout: 5000,
                  layout: 'topRight',
                  text: '<h3>Se ha preparado la factura</h3>'
                }).show();
              }).fail(function(err) {
                new Noty({
                  theme: 'bootstrap-v4',
                  type: 'warning',
                  timeout: 5000,
                  layout: 'topRight',
                  text: '<h3>No se ha podido configurar para factura</h3>'
                }).show();
              })
            } else {
              new Noty({
                theme: 'bootstrap-v4',
                type: 'success',
                timeout: 5000,
                layout: 'topRight',
                text: '<h3>Se ha preparado la factura</h3>'
              }).show();
            }
          }

        }).fail(function(err) {
          new Noty({
            theme: 'bootstrap-v4',
            type: 'warning',
            timeout: 5000,
            layout: 'topRight',
            text: '<h3>Ha caducado el tiempo de configuración</h3>'
          }).show();
          def.reject()
        });

      }, 3000);

      if ($(".js_invoice_second").hasClass("highlight")) {
        order.set_to_invoice(!order.is_to_invoice());
        $(".js_invoice_second").removeClass("highlight");
      }

      order.set_to_invoice(!order.is_to_invoice());
      if (order.is_to_invoice()) {
        this.$('.js_invoice').addClass('highlight');
      } else {
        this.$('.js_invoice').removeClass('highlight');
      }

      function wait(ms)
      {
        var d = new Date();
        var d2 = null;
        do { d2 = new Date(); }
        while(d2-d < ms);
      }
      wait(4000);
    },
    renderElement: function() {
      var self = this;
      this._super();

      this.$('.js_invoice_second').click(function(){
        self.click_invoice_second();
      });
    },
    send_facturalo_request: function(url, token, data) {
      var request = $.ajax({
        url: url,
        method: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        cache: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader(
            "Authorization",
            'Bearer ' + token
          );
          xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
        },
      })
      return $.when(
        request
      )
    },
    save_order_to_sync: function(data) {
      var order = this.pos.get_order();
      data.pos_reference = order.name
      data.session_id = this.pos.pos_session.id
      return $.when(
        this._rpc({
          model: 'pos.order.sync',
          method: 'create',
          args: [data],
        })
      )
    },
    order_is_valid: function(force_validation) {
      var self = this;
      var sup = this._super(force_validation);
      var order = this.pos.get_order();

      if (!sup) {
        return false;
      }

      console.log(order);
      console.log(order.invoicing_second);

      if ($(".js_invoice_second").hasClass("highlight")) {
        order.serial_number = self.pos.config.iface_boleta_serial_number;
        order.document_type_number = '03';
      } else if ($(".js_invoice").hasClass("highlight")) {
        order.serial_number = self.pos.config.iface_factura_serial_number;
        order.document_type_number = '01';
      }

      var queryDone = true;
      var client = order.get_client();

      if (!client) {
        self.gui.show_popup('confirm', {
          title: 'Error en cliente',
          body:  'Debe seleccionar un cliente para avanzar con la venta.',
          confirm: function(){
            self.gui.show_screen('clientlist');
          },
        });
        return false;
      }

      if (!client.catalog_06_id) {
        self.gui.show_popup('confirm', {
          title: 'Error en datos del cliente',
          body:  'El cliente seleccionado no tiene el tipo de documento registrado, por favor actualice la información del cliente.',
          confirm: function(){
            self.gui.show_screen('clientlist');
          },
        });
        return false;
      }


      if (this.pos.config.is_facturalo_api && this.pos.config.facturalo_endpoint_state == 'success') {


        // si monto es mayor a 700  || o || seleccionó factura y monto es menor a 700
        if(order.get_total_with_tax() > '700' || $(".js_invoice").hasClass("highlight") && order.get_total_with_tax() < '700') {


          // si seleccionó factura el cliente debe ser RUC
          if ($(".js_invoice").hasClass("highlight") && client.catalog_06_id[0] != 4) {
            self.gui.show_popup('confirm', {
              title: 'Error en datos del cliente',
              body:  'El cliente seleccionado no posee tipo de documento RUC, por favor actualice la información del cliente o seleccione otro en su lugar.',
              confirm: function(){
                self.gui.show_screen('clientlist');
              },
            });
            return false;
          }

          // si no tiene numero de documento
          if (!client.doc_number) {
            self.gui.show_popup('confirm', {
              title: 'Error en datos del cliente',
              body:  'El cliente seleccionado no tiene el número de documento registrado, por favor actualice la información del cliente.',
              confirm: function(){
                self.gui.show_screen('clientlist');
              },
            });
            return false;
          }


        }

        var doc_type = this.pos.doc_types.filter(function(doc_type) {
              return doc_type.id == client.catalog_06_id[0]
            })[0].code

        // console.log(order)

        var company = this.pos.company;
        var currency = this.pos.currency;

        if (!$(".js_invoice").hasClass("highlight") && !$(".js_invoice_second").hasClass("highlight")) {
          self.gui.show_popup('error', {
            title: 'Error',
            body:  'Debe seleccionar una opción de método de facturación.'
          });
          return false;
        }

        var document_type_number = order.document_type_number;

        var now = moment();
        var order_date_formatted = now.format('YYYY-MM-DD');
        var order_time = now.format('HH:mm:ss');

        var amount_with_tax = order.get_total_with_tax();
        var total_without_tax = order.get_total_without_tax();
        var amount_tax = order.get_total_tax();

        var items = [];
        var totals_discount = 0;
        var totals_igv = 0;

        var orderLines = order.get_orderlines();

        for (var i = 0; i < orderLines.length; i++) {

          //linea
          var line = orderLines[i];
          //producto
          var product = line.get_product();
          // cantidad de producto
          var product_quantity = line.get_quantity();
          // detalles de impuesto
          var taxDetails = line.get_tax_details();

          var product_taxes_total = 0.0;
          for(var id in taxDetails){
            product_taxes_total += taxDetails[id]; //monto total del igv
          }

          // precio de linea / original o asignado
          var price = '';
          if(line.price_manually_set == true){
            price = line.price;
            console.log(true);
          }else {
            price = product.list_price;
          }

          //var taxes_porcentage = Math.round((((price + product_taxes_total) - price) / price) * 100) / product_quantity;
          //var amountIgv = (((price) * (taxes_porcentage)) * product_quantity) / 100;

          //total descuento por linea
          //var total_discount_line = (price * line.discount) / 100;
          //var total_item_value = (price * product_quantity) - total_discount_line;
          //var total_sale_item = total_item_value - line.get_tax();

          /* NUEVAS FORMULAS */
          // monto del descuento basado en porcentaje
          var monto_descuento_unidad = (price * line.discount) / 100;
          // precio por unidad
          var precio_unidad = price - monto_descuento_unidad;
          // cantidad de porcentaje de linea
          var monto_descuento_linea = monto_descuento_unidad * product_quantity;


          // total linea
          var monto_total_linea = precio_unidad * product_quantity;
          // valor unidad sin igv
          var igv_unidad = line.get_tax() / product_quantity;
          var valor_unidad = price - igv_unidad;

          // totales->total_descuentos
          totals_discount = totals_discount + monto_descuento_linea;

          totals_igv = totals_igv + line.get_tax();
          var monto_base = price * product_quantity


          var unidades = this.pos.units;
          var uni = product.uom_id[1];

          var unidad = unidades.find(function(element) {
            return element.display_name == uni;
          });

          var product_uom = unidad.code;
          var total_sin_impuesto = monto_total_linea - line.get_tax();

          items.push({
            "codigo_interno": product.default_code,
            "descripcion": product.display_name,
            "codigo_producto_de_sunat": "51121703",
            "unidad_de_medida": product_uom,
            "cantidad": product_quantity,
            "valor_unitario": valor_unidad.toFixed(2),
            "codigo_tipo_precio": "01",
            "precio_unitario": price,
            "codigo_tipo_afectacion_igv": "10",
            "descuentos":[
              {
                "codigo": "00",
                "descripcion": "Descuento",
                "factor": line.discount,
                "monto": monto_descuento_linea.toFixed(2),
                "base": monto_base.toFixed(2)
              }
            ],
            "total_base_igv": total_sin_impuesto.toFixed(2),
            "porcentaje_igv": line.get_taxes()[0].amount,
            "total_igv": line.get_tax().toFixed(2),
            "total_impuestos": line.get_tax().toFixed(2),
            "total_valor_item": total_sin_impuesto.toFixed(2),
            "total_item": monto_total_linea
          })
        }

        var data = {
          "serie_documento": order.serial_number,
          "numero_documento": order.invoicing_second,
          "fecha_de_emision": order_date_formatted,
          "hora_de_emision": order_time,
          "codigo_tipo_operacion": "0101",
          "codigo_tipo_documento": document_type_number,
          "codigo_tipo_moneda": currency.name,
          "factor_tipo_de_cambio": "3.35",
          "fecha_de_vencimiento": order_date_formatted,
          "numero_orden_de_compra": "-",
          "datos_del_emisor": {
            "codigo_del_domicilio_fiscal": "0000"
          },
          "datos_del_cliente_o_receptor": {
            "codigo_tipo_documento_identidad": doc_type,
            "numero_documento": client.vat,
            "apellidos_y_nombres_o_razon_social": client.name,
            "codigo_pais": "PE",
            "ubigeo": client.ubigeo,
            "direccion": client.address,
          },
          "totales": {
            "total_descuentos": totals_discount,
            "total_operaciones_gravadas": total_without_tax,
            "total_operaciones_inafectas": "0.00",
            "total_operaciones_exoneradas": "0.00",
            "total_operaciones_gratuitas": "0.00",
            "total_igv": totals_igv.toFixed(2),
            "total_impuestos": totals_igv.toFixed(2),
            "total_valor": total_without_tax,
            "total_venta": amount_with_tax.toFixed(2),
          },
          "items": items,
          "leyendas": []
        }

        console.log(data)

        var url = this.pos.config.iface_facturalo_url_endpoint;
        var token = this.pos.config.iface_facturalo_token;
        var def = $.Deferred();
        var json = data;


        this.send_facturalo_request(url, token, data)
          .done(function(data) {
            var data_response = {
              api_number_feapi: data['data']['number'],
              api_external_id: data['data']['external_id'],
              api_link_cdr: data['links']['cdr'],
              api_link_xml: data['links']['xml'],
              api_link_pdf: data['links']['pdf'],
              api_qr: data['data']['qr'],
            }
            var data_to_invoice = {
              number_feapi: data['data']['number'],
              api_number_to_letter: data['data']['number_to_letter'],
              external_id: data['data']['external_id'],
              link_cdr: data['links']['cdr'],
              link_xml: data['links']['xml'],
              link_pdf: data['links']['pdf'],
              api_qr: data['data']['qr'],
              state_api: 'success',
            }

            var domain = [['pos_reference', '=', order.name], ['session_id', '=', self.pos.pos_session.id]];
            setTimeout(function() {
              // busca la orden
              self._rpc({
                model: 'pos.order',
                method: 'search_read',
                domain: domain,
                fields: ['id','invoice_id'],
              }).done(function(o) {


                // si ya se ha creado la factura se busca para guardar los datos aqui
                var search = [['id', '=', o[0].invoice_id[0]]];
                self._rpc({
                  model: 'account.invoice',
                  method: 'search_read',
                  domain: search,
                  fields: ['id'],
                }).done(function(res) {
                  // guarda los datos extras en la factura
                  self._rpc({
                    model: 'account.invoice',
                    method: 'write',
                    args: [[res[0].id], data_to_invoice],
                  }).done(function() {
                    console.log('Se guardaron los datos extras en account.invoice');
                  }).fail(function(err) {
                    console.log('No se guardaron los datos extras en account.invoice');
                  })
                }).fail(function(err) {
                  console.log('no se encontro la factura');
                })


                // si no hay respuesta
                if (o.length == 0) {
                  // guarda en la tabla de los otros sincronizados
                  self.save_order_to_sync(data_response)
                  def.reject()
                } else {
                  // guarda los datos extras en la orden
                  self._rpc({
                    model: 'pos.order',
                    method: 'write',
                    args: [[o[0].id], data_response],
                  }).done(function() {
                    def.resolve()
                  }).fail(function(err) {
                    self.save_order_to_sync(data_response)
                    new Noty({
                      theme: 'bootstrap-v4',
                      type: 'warning',
                      timeout: 10000,
                      layout: 'bottomRight',
                      text: '<h3>La operación ha sido registrada con algunos incovenientes en el pedido, para actualizar el pedido debe dirigirse al módulo de punto de venta e ingresar en el menú «Pedidos por sincronizar» y ejecutar las acciones pertinentes.</h3>'
                    }).show();
                    def.reject()
                  })

                  //guardo el json
                  var data_json = {
                    api_code_json: JSON.stringify(json, null, ' ')
                  }
                  self._rpc({
                    model: 'pos.order',
                    method: 'write',
                    args: [[o[0].id], data_json],
                  }).done(function() {
                    def.resolve()
                  }).fail(function(err) {
                    new Noty({
                      theme: 'bootstrap-v4',
                      type: 'warning',
                      timeout: 10000,
                      layout: 'bottomRight',
                      text: '<h3>No se ha almacenado el archivo Json generado para el envio hacia la API</h3>'
                    }).show();
                    def.reject()
                  })
                }

              }).fail(function(err) {
                self.save_order_to_sync(data_response)

                new Noty({
                  theme: 'bootstrap-v4',
                  type: 'warning',
                  timeout: 10000,
                  layout: 'bottomRight',
                  text: '<h3>La operación ha sido registrada con algunos incovenientes en el pedido, para actualizar el pedido debe dirigirse al módulo de punto de venta e ingresar en el menú «Pedidos por sincronizar» y ejecutar las acciones pertinentes.</h3>'
                }).show();
                def.reject()
              })

            }, 3000);

            new Noty({
              theme: 'bootstrap-v4',
              type: 'success',
              timeout: 3000,
              layout: 'bottomRight',
              text: '<h3>La operación ha finalizado.</h3>'
            }).show();


            if (data['data']['number'].charAt(0) == 'B') {
              $('#receipt-type').append('BOLETA ELECTRÓNICA')
            } else {
              $('#receipt-type').append('FACTURA ELECTRÓNICA')
            }

            $('#receipt-serie-number').append(data['data']['number'])

            $('#number-to-letter').append(data['data']['number_to_letter'])

            $('#hash').append(data['data']['hash'])

            var urlcorta = url.indexOf('/api');
            var newUrl = url.substring(0, urlcorta+1) + 'search';

            $('#search').html(newUrl)

            $('#qr').html(
              '<img style="margin-top:15px;" class="qr_code" src="data:image/png;base64, ' + data['data']['qr'] + '" />'
            )

            $('#logo').html(
              '<img style="margin-top:15px;" src="' + order['pos']['company_logo']['currentSrc'] + '" />'
            )

            var typedoc = ''
            if (order['attributes']['client'] !== null) {
              if (order['attributes']['client']['catalog_06_id'][0] == 1) {
                typedoc = 'DOC.TRIB.NO.DOM.SIN.RUC';
              } else if (order['attributes']['client']['catalog_06_id'][0] == 2) {
                typedoc = 'DNI';
              }else if (order['attributes']['client']['catalog_06_id'][0] == 3) {
                typedoc = 'Carnet de Extrangería';
              }else if (order['attributes']['client']['catalog_06_id'][0] == 4) {
                typedoc = 'RUC';
              }else if (order['attributes']['client']['catalog_06_id'][0] == 5) {
                typedoc = 'Pasaporte';
              }else if (order['attributes']['client']['catalog_06_id'][0] == 6) {
                typedoc = 'Cédula Diplomática de Identidad';
              }else if (order['attributes']['client']['catalog_06_id'][0] == 7) {
                typedoc = 'Varios';
              } else {
                typedoc = '';
              }
            }

            $('#client-doc-type').append(typedoc)

            queryDone = true;


          }).fail(function(xhr, status, error) {
            def.reject()
            // console.log(status)
            if (xhr.responseText != '') {
              var err = eval("(" + xhr.responseText + ")");
              self.gui.show_popup('error', {
                title: 'ERROR: Ha ocurrido un error en el envío',
                body: err.message,
              });
              return false;
            } else {
              self.gui.show_popup('error', {
                title: 'ERROR: Ha ocurrido un error en el envío',
                body: 'No se ha obtenido respuesta del servidor',
              });
              return false;
            }
          })
      } else {
        new Noty({
          theme: 'bootstrap-v4',
          type: 'warning',
          timeout: 3000,
          layout: 'bottomRight',
          text: '<h3>No se ecuentra conectado con el facturador.</h3>'
        }).show();
      }

      if (queryDone != true) {
        return false;
      }

      return true
    },
  });
});

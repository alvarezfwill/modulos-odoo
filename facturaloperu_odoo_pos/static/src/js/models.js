odoo.define('facturaloperu_odoo_pos.models', function(require) {
  "use strict";

    var models = require("point_of_sale.models");
    models.load_fields('pos.config',
        ['print_pdf']
    );

    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({

        push_and_invoice_order: function (order) {
            var self = this;
            var invoiced = new $.Deferred();

            if (order) {
                var order_id = this.db.add_order(order.export_as_JSON());
            }

            this.flush_mutex.exec(function () {
                var done = new $.Deferred();

                var transfer = self._flush_orders(self.db.get_orders(), {
                    timeout: 30000,
                    to_invoice: true
                });

                transfer.fail(function (error) {
                    invoiced.reject(error);
                    done.reject();
                });
                if (self.config.print_pdf) {
                    // on success, get the order id generated by the server
                    transfer.pipe(function (order_server_id) {

                        // generate the pdf and download it
                        self.chrome.do_action('point_of_sale.pos_invoice_report', {
                            additional_context: {
                                active_ids: order_server_id,
                            }
                        }).done(function () {
                            invoiced.resolve();
                            done.resolve();
                        });
                    });
                } else {
                    invoiced.resolve();
                    done.resolve();
                }
                return done;
            });
            return invoiced;
        }
    });
});
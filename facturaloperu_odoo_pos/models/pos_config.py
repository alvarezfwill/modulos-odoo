from odoo import (
    api,
    fields,
    models
)
import requests


class PosConfig(models.Model):
    _inherit = 'pos.config'

    is_facturalo_api = fields.Boolean(
        string='Facturalo Peru API'
    )
    iface_facturalo_url_endpoint = fields.Char(
        string='Endpoint url for Facturalo API'
    )
    iface_facturalo_token = fields.Char(
        string='Token for Facturalo API'
    )
    facturalo_endpoint_state = fields.Selection(
        [('draft', 'Draft'), ('success', 'Success'), ('error', 'Error')],
        string='Is the URL endpoint valid?',
        default='draft'
    )

    iface_factura_serial_number = fields.Char(
        string='Número de serie de Factura'
    )
    iface_boleta_serial_number = fields.Char(
        string='Número de serie de Boleta'
    )

    iface_invoicing_second = fields.Boolean(
        string='Boleta',
        help='Habilitar boletas desde Punto de Venta.'
    )
    invoice_second_journal_id = fields.Many2one(
        'account.journal', string='Diario para Boleta',
        domain=[('type', '=', 'sale')],
        help="Diario contable para crear boletas.",
    )

    invoicing_second = fields.Text(
        string='invoicing_second',
        readonly=True
    )

    print_pdf = fields.Boolean(
        string="Imprimir PDF enviado a Facturación",
        default=False
    )

    @api.onchange('iface_facturalo_url_endpoint', 'iface_facturalo_token')
    def set_valid_endpoint(self):
        self.facturalo_endpoint_state = 'draft'

    @api.onchange('invoice_second_journal_id')
    def _onchange_invoice_second_journal_id(self):
        if self.invoice_second_journal_id:
            serie = self.invoice_second_journal_id.code
            self.iface_boleta_serial_number = serie

    @api.onchange('invoice_journal_id')
    def _onchange_invoice_journal_id(self):
        if self.invoice_journal_id:
            serie = self.invoice_journal_id.code
            self.iface_factura_serial_number = serie

    #@api.onchange('iface_facturalo_url_endpoint', 'iface_facturalo_token')
    #def set_iface_factura_Serial_number(self):
    #    self.facturalo_endpoint_state = 'draft'

    @api.multi
    def test_facturalo_api_connection(self):
        self.ensure_one()
        headers = {'Authorization': 'Bearer ' + self.iface_facturalo_token}

        r = requests.post(
            self.iface_facturalo_url_endpoint,
            data={},
            headers=headers
        )
        if r.status_code == 404:
            self.facturalo_endpoint_state = 'error'
        else:
            self.facturalo_endpoint_state = 'success'

from odoo import (
    api,
    models,
    fields,
    exceptions
)

from requests.exceptions import ConnectionError, HTTPError, Timeout
import requests


class PosFacturaloApi(models.Model):
    _inherit = 'pos.order'

    api_external_id = fields.Char(
        string='External Id',
        readonly=True
    )
    api_number_feapi = fields.Char(
        string='Número',
        readonly=True
    )
    api_link_cdr = fields.Char(
        string='CDR',
        readonly=True
    )
    api_link_pdf = fields.Char(
        string='PDF',
        readonly=True
    )
    api_link_xml = fields.Char(
        string='XML',
        readonly=True
    )
    api_code_json = fields.Text(
        string='JSON',
        readonly=True
    )

    invoicing_second = fields.Text(
        string='invoicing_second',
        readonly=True
    )

    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a pos order.
        """
        # raise exceptions.Warning(self.invoicing_second)


        # eljournal = self.session_id.config_id.invoice_journal_id.id if self.invoicing_second == 'dos' else self.session_id.config_id.invoice_second_journal_id.id

        invoice_type = 'out_invoice' if self.amount_total >= 0 else 'out_refund'
        return {
            'name': self.name,
            'origin': self.name,
            'account_id': self.partner_id.property_account_receivable_id.id,
            'journal_id': self.session_id.config_id.invoice_journal_id.id,
            'company_id': self.company_id.id,
            'type': invoice_type,
            'reference': self.name,
            'partner_id': self.partner_id.id,
            'comment': self.note or '',
            #'state_api': 'success',
            # considering partner's sale pricelist's currency
            'currency_id': self.pricelist_id.currency_id.id,
            'user_id': self.user_id.id,
        }